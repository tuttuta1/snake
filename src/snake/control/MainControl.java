package snake.control;

import snake.model.MainModel;
import snake.view.MainWindow;

/**
 * Instancie les autres controlleurs.
 */

public abstract class MainControl {
    protected static MainWindow actualWindow;
    protected static MainModel actualModel;

    public static void initControl(MainModel model) {
        actualModel = model;
        actualWindow = new MainWindow(model);
        new ControlMenu();
        new ControlHelp();
        new ControlGame();
        new ControlLose();
        new ControlHigh();
    }
}
