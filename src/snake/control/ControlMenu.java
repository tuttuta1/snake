package snake.control;

import snake.model.GlobalConstants;
import snake.view.panels.PanelMenu;

import java.awt.event.*;

/**
 * Controlleur du menu
 */

public class ControlMenu extends MainControl implements MouseMotionListener, ActionListener, MouseListener, GlobalConstants {

    public ControlMenu() {
        actualWindow.getMenuPanel().setMenuControler(this);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!actualModel.getMenu().getAnimationDuring()) {
            if (actualModel.getMenu().getPlayIsActivated() && e.getSource() != actualWindow.getMenuPanel().helpButton) {
                switch (e.getActionCommand().charAt(0)) {
                    case '1':
                        actualModel.newGame(1); // Commence une partie à 1 joueur
                        actualWindow.getGamePanel().updateModel(actualModel);
                        actualWindow.getMenuPanel().initStepButton();
                        actualWindow.getMenuPanel().setHoover(0, false);
                        actualWindow.getGamePanel().previousPanel = actualWindow.getMenuPanel().getColor();
                        actualModel.getMenu().setTimerState(false);
                        actualWindow.switchPanel(actualWindow.getGamePanel());
                        break;
                    case '2':
                        actualModel.newGame(2); // Commence une partie à 2 joueur
                        actualWindow.getGamePanel().updateModel(actualModel);
                        actualWindow.getMenuPanel().initStepButton();
                        actualWindow.getMenuPanel().setHoover(1, false);
                        actualWindow.getGamePanel().previousPanel = actualWindow.getMenuPanel().getColor();
                        actualModel.getMenu().setTimerState(false);
                        actualWindow.switchPanel(actualWindow.getGamePanel());
                        break;
                    case '3':
                        actualModel.getMenu().setAnimationDuring(true);
                        break;
                }
            } else if (e.getSource() != actualWindow.getMenuPanel().helpButton) {
                switch (e.getActionCommand().charAt(0)) {
                    case '1':
                        actualWindow.switchPanel(actualWindow.getHighScorePanel());
                        actualWindow.getHighScorePanel().color = actualWindow.getMenuPanel().getColor();
                        break;
                    case '2':
                        actualModel.getMenu().setAnimationDuring(true);
                        break;
                    case '3':
                        System.exit(0);
                        break;
                }
            } else {
                actualWindow.switchPanel(actualWindow.getHelpPanel());
                actualWindow.getHelpPanel().color = actualWindow.getMenuPanel().getColor();
            }

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Object source = e.getSource();
        PanelMenu menu = actualWindow.getMenuPanel();
        if (source == menu.button1)
            menu.setHoover(0, true);
        else if (source == menu.button2)
            menu.setHoover(1, true);
        else if (source == menu.button3)
            menu.setHoover(2, true);
        else
            menu.setHelpHoover(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        Object source = e.getSource();
        PanelMenu menu = actualWindow.getMenuPanel();
        if (source == menu.button1)
            menu.setHoover(0, false);
        else if (source == menu.button2)
            menu.setHoover(1, false);
        else if (source == menu.button3)
            menu.setHoover(2, false);
        else
            menu.setHelpHoover(false);

    }
}
