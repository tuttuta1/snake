package snake.control;

import snake.view.panels.PanelLose;

import java.awt.event.*;

/**
 * Controlleur du panelLose
 */

public class ControlLose extends MainControl implements MouseMotionListener, ActionListener, MouseListener {
    public ControlLose() {
        PanelLose lose = actualWindow.getLosePanel();
        lose.setMouseListener(this);
        lose.setActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        actualModel.getLose().animMasque = true;
        if (e.getSource() == actualWindow.getLosePanel().newGame)
            actualModel.getLose().n = 1;
        else {
            actualModel.getLose().n = 2;
            actualModel.getMenu().musicMenu.play();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Object source = e.getSource();
        PanelLose lose = actualWindow.getLosePanel();
        if (source == lose.newGame)
            actualModel.getLose().hoover[0] = true;
        else
            actualModel.getLose().hoover[1] = true;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        Object source = e.getSource();
        PanelLose lose = actualWindow.getLosePanel();
        if (source == lose.newGame)
            actualModel.getLose().hoover[0] = false;
        else
            actualModel.getLose().hoover[1] = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
