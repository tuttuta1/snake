package snake.control.timers;

import snake.control.MainControl;
import snake.lib.GameTickCounter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameTimer extends MainControl implements ActionListener {
    private final GameTickCounter beforeEnd;


    public GameTimer() {
        this.beforeEnd = new GameTickCounter(15);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (actualModel.getGame().isEnd()) {
            actualModel.getGame().musicGame.stop();
            actualModel.getGame().musicGame.beginning();
            if (beforeEnd.isUp()) {
                return;
            } else if (actualWindow.getContentPane() == actualWindow.getGamePanel()) {
                actualWindow.getLosePanel().updateScore();
                actualWindow.switchPanel(actualWindow.getLosePanel());
                actualModel.getGame().setTimerState(false);
            }
        } else
            actualModel.getGame().update();
        actualWindow.getGamePanel().repaint();
    }
}
