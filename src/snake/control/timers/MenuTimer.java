package snake.control.timers;

import snake.control.MainControl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuTimer extends MainControl implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        actualModel.getMenu().update();
        actualWindow.getMenuPanel().update();
        actualWindow.getMenuPanel().setColor(actualModel.getMenu().getColor());
        actualWindow.getMenuPanel().repaint();
        actualWindow.getHighScorePanel().setColor(actualModel.getMenu().getColor());
        actualWindow.getHighScorePanel().repaint();
        actualWindow.getHelpPanel().setColor(actualModel.getMenu().getColor());
        actualWindow.getHelpPanel().repaint();

    }
}
