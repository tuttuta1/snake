package snake.control.timers;

import snake.control.MainControl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoseTimer extends MainControl implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        actualWindow.getLosePanel().update();
        actualWindow.getLosePanel().repaint();

    }
}
