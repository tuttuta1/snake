package snake.control;

import snake.model.GlobalConstants;

import java.awt.event.*;

/**
 * Controlleur de l'highScore.
 */

public class ControlHigh extends MainControl implements MouseMotionListener, ActionListener, MouseListener, GlobalConstants {
    public ControlHigh() {
        actualWindow.getHighScorePanel().setHighControler(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        actualWindow.switchPanel(actualWindow.getMenuPanel());
        actualWindow.getMenuPanel().setHoover(0, false);
        actualModel.getMenu().reset();
        actualModel.getMenu().backHighscoreHoover = false;

        actualWindow.getMenuPanel().initStepButton();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == actualWindow.getHighScorePanel().backbutton) {
            actualModel.getMenu().backHighscoreHoover = true;

        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == actualWindow.getHighScorePanel().backbutton) {
            actualModel.getMenu().backHighscoreHoover = false;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
