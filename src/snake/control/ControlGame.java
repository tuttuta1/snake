package snake.control;

import snake.model.GameModel;
import snake.model.exception.NotAPlayableSnake;
import snake.view.ViewConstants;

import java.awt.*;
import java.awt.event.*;

/**
 * Controlleur du jeu.
 */

public class ControlGame extends MainControl implements KeyListener, ActionListener, MouseListener {

    public ControlGame() {
        actualWindow.getGamePanel().setKeyControler(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        GameModel gamemodel = actualModel.getGame();
        if (!actualModel.getGame().isLaunched()) {
            actualModel.getGame().setLaunched(true);
            actualModel.getGame().setTimerState();
        }

        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case 0x42:
                if (gamemodel.bonusvar == 0) gamemodel.bonusvar++;
                break;
            case 0x4F:
                if (gamemodel.bonusvar == 1) gamemodel.bonusvar++;
                break;
            case 0x4E:
                if (gamemodel.bonusvar == 2) gamemodel.bonusvar++;
                break;
            case 0x55:
                if (gamemodel.bonusvar == 3) gamemodel.bonusvar++;
                break;
            case 0x53:
                if (gamemodel.bonusvar == 4) gamemodel.bonusvar = 42;
                break;
        }
        if (gamemodel.bonusvar >= 38 && gamemodel.bonusvar <= 42)
            actualModel.getGame().setBonus(true);

        switch (keyCode) {
            case 0x42:
                if (gamemodel.bonusvar == 42) gamemodel.bonusvar--;
                break;
            case 0x4F:
                if (gamemodel.bonusvar == 41) gamemodel.bonusvar--;
                break;
            case 0x4E:
                if (gamemodel.bonusvar == 40) gamemodel.bonusvar--;
                break;
            case 0x55:
                if (gamemodel.bonusvar == 39) gamemodel.bonusvar--;
                break;
            case 0x53:
                if (gamemodel.bonusvar == 38) gamemodel.bonusvar = 0;
                break;
        }

        if (gamemodel.bonusvar >= 0 && gamemodel.bonusvar <= 4)
            actualModel.getGame().setBonus(false);


        if (!actualModel.getGame().isPaused())
            switch (keyCode) {
                case KeyEvent.VK_ESCAPE:
                case KeyEvent.VK_P:
                    actualWindow.getGamePanel().menu_button.setVisible(true);
                    actualModel.getGame().setPaused(true);
                    actualModel.getGame().setTimerState(false);
                    actualWindow.repaint();
                    actualModel.getGame().musicGame.stop();
                    break;
            }


        if (actualModel.getGame().isPaused())
            switch (keyCode) {
                case KeyEvent.VK_Z:         //Fleche du haut du joueur 2
                case KeyEvent.VK_Q:         //Fleche de gauche du joueur 2
                case KeyEvent.VK_D:         //Fleche de droite du joueur 2
                case KeyEvent.VK_UP:        //Fleche du haut
                case KeyEvent.VK_LEFT:      //Fleche de gauche
                case KeyEvent.VK_RIGHT:     //Fleche de droite
                    actualWindow.getGamePanel().menu_button.setVisible(false);
                    actualModel.getGame().setPaused(false);
                    actualModel.getGame().setTimerState();
                    actualModel.getGame().musicGame.play();
                    break;
            }
        else {
            try {
                switch (keyCode) {
                    case KeyEvent.VK_UP:        //Fleche du haut
                        actualModel.getGame().getPlayers(0).accelerate(true);
                        break;
                    case KeyEvent.VK_LEFT:      //Fleche de gauche
                        actualModel.getGame().getPlayers(0).rotate('l');
                        break;
                    case KeyEvent.VK_RIGHT:    //Fleche de droite
                        actualModel.getGame().getPlayers(0).rotate('r');
                        break;
                }
                if (actualModel.getGame().players.length > 1) {
                    switch (keyCode) {
                        case KeyEvent.VK_Z:        //Fleche du haut du joueur 2
                            actualModel.getGame().getPlayers(1).accelerate(true);
                            break;
                        case KeyEvent.VK_Q:      //Fleche de gauche du joueur 2
                            actualModel.getGame().getPlayers(1).rotate('l');
                            break;
                        case KeyEvent.VK_D:    //Fleche de droite du joueur 2
                            actualModel.getGame().getPlayers(1).rotate('r');
                            break;
                    }
                }
            } catch (NotAPlayableSnake exception) {
                exception.printStackTrace();
            }
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        try {
            switch (keyCode) {
                case KeyEvent.VK_UP:        //Fleche du haut
                    actualModel.getGame().getPlayers(0).accelerate(false);
                    break;
                case KeyEvent.VK_LEFT:      //Fleche de gauche
                case KeyEvent.VK_RIGHT:    //Fleche de droite
                    actualModel.getGame().getPlayers(0).rotate('n');
                    break;
            }
            if (actualModel.getGame().players.length > 1)
                switch (keyCode) {
                    case KeyEvent.VK_Z:        //Fleche du haut
                        actualModel.getGame().getPlayers(1).accelerate(false);
                        break;
                    case KeyEvent.VK_Q:      //Fleche de gauche
                    case KeyEvent.VK_D:    //Fleche de droite
                        actualModel.getGame().getPlayers(1).rotate('n');
                        break;
                }

        } catch (NotAPlayableSnake exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == actualWindow.getGamePanel().menu_button) {
            actualWindow.getGamePanel().menu_button.setVisible(false);
            actualModel.getMenu().setTimerState(true);
            actualModel.getMenu().reset();
            actualWindow.switchPanel(actualWindow.getMenuPanel());
            actualModel.getGame().musicGame.stop();
            actualModel.getGame().musicGame.beginning();
            actualModel.getMenu().musicMenu.play();
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        actualModel.getGame().hoover = true;
        if (e.getSource() == actualWindow.getGamePanel().menu_button) {
            actualWindow.getGamePanel().colorMenu = Color.WHITE;
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        actualModel.getGame().hoover = false;
        if (e.getSource() == actualWindow.getGamePanel().menu_button) {
            actualWindow.getGamePanel().colorMenu = ViewConstants.HOVER_COLOR;
        }
    }
}
