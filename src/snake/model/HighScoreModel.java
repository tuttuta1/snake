package snake.model;

import snake.model.exception.NotCreatedScoreFile;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Classe de gestion des meilleurs scores.
 */

public class HighScoreModel {

    private Font scoresFontSize;
    private Font scoresFontSize2;
    private Font TitreScoreSize;
    private Font backButtonSize;

    public int[] scoresSolo;
    public int[] scoresMulti;

    public HighScoreModel() {
        try {
            Font mainFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Light.ttf"));
            Font boldFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Bold.ttf"));
            scoresFontSize = mainFont.deriveFont(60f);
            scoresFontSize2 = boldFont.deriveFont(30f);
            backButtonSize = mainFont.deriveFont(40f);
            TitreScoreSize = boldFont.deriveFont(50f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Font getScoresFontSize() {
        return scoresFontSize;
    }

    public Font getScoresFontSize2() {
        return scoresFontSize2;
    }

    public Font getTitreScoreSize() {
        return TitreScoreSize;
    }

    public Font getBackButtonSize() {
        return backButtonSize;
    }

    public void scoreMulti() throws NotCreatedScoreFile {
        scoresMulti = MainModel.lireScore("multi");
    }

    public void scoreSolo() throws NotCreatedScoreFile {
        scoresSolo = MainModel.lireScore("solo");
    }
}
