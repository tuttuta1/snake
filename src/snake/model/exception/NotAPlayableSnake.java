package snake.model.exception;

/**
 * Erreur si le snake n'existe pas
 */
public class NotAPlayableSnake extends Exception {
    public NotAPlayableSnake(int n) {
        super("Snake number " + n + " is not a playable snake.");
    }
}
