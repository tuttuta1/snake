package snake.model.exception;

/**
 * Erreur si les fichiers de scores n'existent pas.
 */
public class NotCreatedScoreFile extends Exception {
    public NotCreatedScoreFile(String kind) {
        super("Le fichier " + kind + ".txt n'existe pas.");
    }
}
