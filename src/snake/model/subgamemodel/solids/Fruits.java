package snake.model.subgamemodel.solids;

import snake.lib.GameTickCounter;
import snake.model.GlobalConstants;

/**
 * Créé un fruit avec sa position, sa vie et son nombre de calories.
 */

public class Fruits extends SolidCircle implements GlobalConstants {

    public final byte calories;
    private final GameTickCounter life;

    public Fruits(short x, short y, byte calories, short lifeDuration) {
        super(x, y, FRUITS_RAY);
        this.calories = calories;
        this.life = new GameTickCounter(lifeDuration);
    }

    public void update() {
        this.life.update();
    }

    public boolean isEatable() {
        return this.life.isActive();
    }

    public void eat() {
        life.set();
    }

}
