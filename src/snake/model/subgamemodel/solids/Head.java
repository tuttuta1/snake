package snake.model.subgamemodel.solids;

import snake.lib.GameTickCounter;
import snake.model.GlobalConstants;
import snake.model.subgamemodel.GameConstants;
import snake.model.subgamemodel.Snake;

import static java.lang.Math.*;

/**
 * Créé la tête du snake avec sa position et son angle.
 */

public class Head extends SolidCircle implements GlobalConstants, GameConstants {
    private GameTickCounter put;
    private double angle;
    private Snake body;
    private double innerX, innerY;

    public Head(short x, short y, double angle, Snake body) {
        super(x, y, HEAD_RAY);
        innerX = x;
        innerY = y;
        this.body = body;
        this.angle = angle;
        this.put = new GameTickCounter();
    }

    public boolean collideWith(Tail other) {
        int difX = (int) (this.x + cos(angle) * ray) - other.x;
        int difY = (int) (this.y + sin(angle) * ray) - other.y;
        return (difX * difX + difY * difY) < (other.ray * other.ray);
    }

    public void move(boolean isAccelerate) {
        if (!body.isDead()) {
            int speed = isAccelerate ? SPEED * 2 : SPEED;
            innerX += cos(angle) * speed;
            innerY += sin(angle) * speed;
            if (innerX < SNAKE_THOR_MARGIN_NEG)
                innerX += SNAKE_THOR_MARGIN_WIDTH - SNAKE_THOR_MARGIN_NEG;
            else if (innerX >= SNAKE_THOR_MARGIN_WIDTH)
                innerX -= SNAKE_THOR_MARGIN_WIDTH - SNAKE_THOR_MARGIN_NEG;
            if (innerY < SNAKE_THOR_MARGIN_NEG)
                innerY += SNAKE_THOR_MARGIN_HEIGHT - SNAKE_THOR_MARGIN_NEG;
            else if (innerY >= SNAKE_THOR_MARGIN_HEIGHT)
                innerY -= SNAKE_THOR_MARGIN_HEIGHT - SNAKE_THOR_MARGIN_NEG;
            x = (short) innerX;
            y = (short) innerY;
        }
    }

    public void rotate(char side) {
        switch (side) {
            case 'r':
                angle += ROTATION_SPEED;
                if (angle > PI) angle = -PI;
                break;
            case 'l':
                angle -= ROTATION_SPEED;
                if (angle < -PI) angle = PI;
                break;
        }
    }

    public void grow() {
        if (!put.isUp()) {
            this.body.tails.add(new Tail(x, y, this.body.snakeLong));
            put.set(TICK_TO_GROW);
        }
    }

    public void update(boolean isAccelerate) {
        grow();
        if (isAccelerate)
            grow();
        move(isAccelerate);
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void dispose() {
        put = null;
        body = null;
    }
}
