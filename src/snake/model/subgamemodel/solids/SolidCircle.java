package snake.model.subgamemodel.solids;

/**
 * Class de gestion de cercles solides.
 */
public abstract class SolidCircle {
    protected short x, y, ray;

    protected SolidCircle(short x, short y, short ray) {
        this.x = x;
        this.y = y;
        this.ray = ray;
    }

    public boolean collideWith(SolidCircle other) {
        int difX = this.x - other.x;
        int difY = this.y - other.y;
        int sumRay = this.ray + other.ray;
        return (difX * difX + difY * difY) < (sumRay * sumRay);
    }

    public boolean collideWith(int x, int y, int ray) {
        int difX = this.x - x;
        int difY = this.y - y;
        int sumRay = this.ray + ray;
        return (difX * difX + difY * difY) < (sumRay * sumRay);
    }

    public short getY() {
        return y;
    }

    public short getX() {
        return x;
    }

    public short getRay() {
        return ray;
    }
}
