package snake.model.subgamemodel.solids;

import snake.model.GlobalConstants;

/**
 * Class de mur du Snake.
 */
public class Wall extends SolidCircle implements GlobalConstants {

    public Wall(short x, short y) {
        super(x, y, RAY_WALL);
    }

}
