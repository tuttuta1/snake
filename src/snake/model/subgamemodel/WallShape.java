package snake.model.subgamemodel;

import java.util.Random;

/**
 * Classe de calcul de la forme des murs
 */
public enum WallShape implements GameConstants {
    SINa, COSa, SINb, COSb, EXP, LIN, ABS;

    private static final double PId2 = Math.PI / 2;

    public static WallShape getRandomShape(Random random) {
        int ordinal = random.nextInt(values().length);
        for (WallShape shape : values())
            if (shape.ordinal() == ordinal)
                return shape;
        return SINa;
    }

    public int getComputedValue(float in, int amplitude) {
        switch (this) {
            case SINa:
                return (int) (Math.sin(in * PId2) * amplitude);
            case COSa:
                return (int) (Math.cos(in * PId2) * amplitude);
            case SINb:
                return (int) (Math.sin(in * PId2 + PId2) * amplitude);
            case COSb:
                return (int) (Math.cos(in * PId2 + PId2) * amplitude);
            case EXP:
                return (int) (Math.exp(in) * amplitude);
            case ABS:
                return (int) (Math.abs(in - 0.5) * amplitude);
            case LIN:
            default:
                return (int) (in * amplitude);
        }
    }
}
