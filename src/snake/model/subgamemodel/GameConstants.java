package snake.model.subgamemodel;

import snake.model.GlobalConstants;

/**
 * Constantes de jeux.
 */
public interface GameConstants {
    public static final byte SNAKE_THOR_MARGIN_NEG = -GlobalConstants.HEAD_RAY * 2 - 1;
    public static final short SNAKE_THOR_MARGIN_WIDTH = GlobalConstants.WINDOW_WIDTH - SNAKE_THOR_MARGIN_NEG;
    public static final short SNAKE_THOR_MARGIN_HEIGHT = GlobalConstants.WINDOW_HEIGHT - SNAKE_THOR_MARGIN_NEG;

    public static final byte SNAKE_GENERATION_MARGIN = 30;
    public static final byte SNAKE_GENERATION_MARGIN_FOR_OTHER = GlobalConstants.HEAD_RAY + SNAKE_GENERATION_MARGIN;

    public static final byte FRUITS_GENERATION_MARGIN_FOR_FRUITS = 34;
    public static final byte FRUITS_GENERATION_MARGIN_FOR_WALL = 32;
    public static final byte FRUITS_GENERATION_MARGIN_FOR_HEAD = 35;

    public static final byte TAIL_LIFE_DURATION_INIT = 60;

    public static final byte SCORE_FRUITS_FACTOR = 3;

    public static final byte SPEED = 2;
    public static final byte TICK_TO_GROW = 7;

    public static final byte N_WALL_LINE_MIN = 2;
    public static final byte N_WALL_LINE_MAX = 4;
    public static final short X_WALL_LINE_AREA_MARGIN = (short) (GlobalConstants.WINDOW_WIDTH / 10);
    public static final short Y_WALL_LINE_AREA_MARGIN = (short) (GlobalConstants.WINDOW_HEIGHT / 10);
    public static final short AMPLITUDE_WALL_LINE_MIN = 120;
    public static final short AMPLITUDE_WALL_LINE_MAX = 160;
    public static final float COEFFICIENT_N_WALL = 0.6f;

    public static final double ROTATION_SPEED = Math.PI / 48;

    public static final byte MIN_CALORIES = 4;
    public static final byte MAX_CALORIES = 21;
    public static final float DURATION_FACTOR = 1.9f;
    public static final short DURATION_MAX = 500;
    public static final byte FRUITS_NUMBER_MODULATION = 1;
}
