package snake.model.subgamemodel;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

/**
 * Classe d'utilisation des sons (manger et mourir).
 */

public class Sound extends Thread {
    private static final int EXTERNAL_BUFFER_SIZE = 524288; // 128Kb

    private final String filename;

    public Sound(String wavFile) {
        filename = wavFile;
    }

    public void run() {
        AudioInputStream audioInputStream;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(Sound.class.getResource(filename));
        } catch (UnsupportedAudioFileException e1) {
            e1.printStackTrace();
            return;
        } catch (IOException e1) {
            e1.printStackTrace();
            return;
        }

        AudioFormat format = audioInputStream.getFormat();
        SourceDataLine auLine;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

        try {
            auLine = (SourceDataLine) AudioSystem.getLine(info);
            auLine.open(format);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        auLine.start();
        int nBytesRead = 0;
        byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];

        try {
            while (nBytesRead != -1) {
                nBytesRead = audioInputStream.read(abData, 0, abData.length);
                if (nBytesRead >= 0) {
                    auLine.write(abData, 0, nBytesRead);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            auLine.drain();
            auLine.close();
        }
    }

}