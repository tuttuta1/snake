package snake.model.subgamemodel;

import snake.model.GlobalConstants;
import snake.model.subgamemodel.solids.Wall;

import java.util.Random;

/**
 * Générateur de mur
 */
public class WallLine implements GameConstants, GlobalConstants {
    private final Wall[] walls;

    public WallLine(Random random) {
        final int AMPLITUDE = AMPLITUDE_WALL_LINE_MIN + random.nextInt(AMPLITUDE_WALL_LINE_MAX - AMPLITUDE_WALL_LINE_MIN);
        walls = new Wall[(int) (AMPLITUDE * COEFFICIENT_N_WALL)];
        final int origX = X_WALL_LINE_AREA_MARGIN + random.nextInt(WINDOW_WIDTH - X_WALL_LINE_AREA_MARGIN);
        final int origY = Y_WALL_LINE_AREA_MARGIN + random.nextInt(WINDOW_HEIGHT - Y_WALL_LINE_AREA_MARGIN);
        final WallShape shape = WallShape.getRandomShape(random);
        final boolean isXShape = random.nextBoolean();
        final int dirX = (int) ((random.nextInt(1) - 0.5) * 2 * (random.nextInt(2) + 1));
        final int dirY = (int) ((random.nextInt(1) - 0.5) * 2 * (random.nextInt(2) + 1));
        final int WpRAY = WINDOW_WIDTH + RAY_WALL;
        final int HpRAY = WINDOW_HEIGHT + RAY_WALL;

        for (int i = 0; i < walls.length; i++) {
            float coefficient = (float) (i) / (walls.length - 1);
            short x = (short) ((origX + (isXShape ? shape.getComputedValue(coefficient, AMPLITUDE) : coefficient * AMPLITUDE) * dirX) % WpRAY);
            short y = (short) ((origY + (isXShape ? coefficient * AMPLITUDE : shape.getComputedValue(coefficient, AMPLITUDE)) * dirY) % HpRAY);
            if (x < -RAY_WALL)
                x += WpRAY + RAY_WALL;
            if (y < -RAY_WALL)
                y += HpRAY + RAY_WALL;
            walls[i] = new Wall(x, y);
        }
    }

    public Wall[] getWalls() {
        return walls;
    }

    public void dispose() {
        for (int i = 0; i < walls.length; i++)
            walls[i] = null;
    }
}
