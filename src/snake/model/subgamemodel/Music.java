package snake.model.subgamemodel;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

/**
 * Classe d'utilisation des musiques.
 */

public class Music {
    private Clip clip;

    public Music(String filename) {
        try {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(Music.class.getResource(filename));

            this.clip = AudioSystem.getClip();

            clip.open(audioIn);

        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void play() {
        this.clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        this.clip.stop();
    }

    public void beginning() {
        this.clip.setMicrosecondPosition(0);
    }
}