package snake.model.subgamemodel;

/**
 * Causes de mort. Les coeff permettent de modifier le score.
 */

public enum EndCause {
    SELF_EATING(1),
    SMASH_A_WALL(0.99f),
    EAT_THE_ENEMIES(0.9f);

    private final float scoreCoef;

    private EndCause(float scoreCoef) {
        this.scoreCoef = scoreCoef;
    }

    public float getScoreMultiplicant() {
        return scoreCoef;
    }
}
