package snake.model;

/**
 * Constantes globales.
 */
public interface GlobalConstants {
    public static final byte HEAD_RAY = 15;
    public static final byte TAIL_RAY = 13;
    public static final byte FRUITS_RAY = 15;
    public static final byte RAY_WALL = 4;

    public static final short WINDOW_WIDTH = 16 * 63;
    public static final short WINDOW_HEIGHT = 9 * 63;
    public static final byte FRAME_RATE = 16;
}
