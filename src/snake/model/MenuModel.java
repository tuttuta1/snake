package snake.model;

import snake.control.timers.MenuTimer;
import snake.lib.GameTickCounter;
import snake.model.subgamemodel.Music;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Modele du menu
 */
public class MenuModel implements GlobalConstants, ViewConstants {
    private boolean playIsActivated;
    private boolean animationDuring;

    private int menuState;
    private final Timer timer;

    private final GameTickCounter r;
    private final GameTickCounter g;
    private final GameTickCounter b;

    public int[] stepButton;
    public float stepBackButton;
    public float stepHelpButton;

    public int[] opacityButton;
    public int[] opacity2Button;

    public int[] rotationButton;
    public int[] rotation2Button;

    public final boolean[] hoover;
    public boolean backHighscoreHoover;
    public boolean helpHoover;

    public final Music musicMenu;

    private Font playFontSize;
    private Font subMenuSize;
    private Font logoMenuSize;
    private Font helpFontSize;


    public MenuModel() {
        timer = new Timer(FRAME_RATE, new MenuTimer());
        opacityButton = new int[]{250, 250, 250};
        opacity2Button = new int[]{0, 0, 0};
        rotationButton = new int[]{-15, 0, 15};
        rotation2Button = new int[]{90, 90, 90};
        stepButton = new int[3];
        hoover = new boolean[3];
        backHighscoreHoover = false;
        helpHoover = false;

        try {
            Font mainFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Light.ttf"));
            Font logoFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Bold.ttf"));
            playFontSize = mainFont.deriveFont(40f);
            subMenuSize = mainFont.deriveFont(30f);
            logoMenuSize = logoFont.deriveFont(45f);
            helpFontSize = mainFont.deriveFont(30f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        musicMenu = new Music("/assets/sound/menu.wav");
        musicMenu.play();

        timer.start();

        int quarterByte = 510 / 3;
        int startPos = (int) (Math.random() * quarterByte);
        r = new GameTickCounter(startPos);
        g = new GameTickCounter(startPos + quarterByte);
        b = new GameTickCounter(startPos + quarterByte + quarterByte);
    }

    public Font getPlayFontSize() {
        return playFontSize;
    }

    public Font getSubMenuSize() {
        return subMenuSize;
    }

    public Font getLogoMenuSize() {
        return logoMenuSize;
    }

    public Font getHelpFontSize() {
        return helpFontSize;
    }

    public void update() {
        if (!r.isUp())
            r.set(510);
        if (!g.isUp())
            g.set(510);
        if (!b.isUp())
            b.set(510);
    }

    public Color getColor() {
        return new Color(Math.abs(r.get() - 255), Math.abs(g.get() - 255), Math.abs(b.get() - 255));
    }

    public void setTimerState(boolean isLaunched) {
        if (isLaunched)
            timer.start();
        else
            timer.stop();
    }

    public boolean getPlayIsActivated() {
        return playIsActivated;
    }

    public void setPlayIsActivated(boolean p) {
        playIsActivated = p;
    }

    public boolean getAnimationDuring() {
        return animationDuring;
    }

    public void setAnimationDuring(boolean p) {
        animationDuring = p;
    }

    public int getMenuState() {
        return menuState;
    }

    public void setMenuState(int state) {
        menuState = state;
    }

    public void reset() {
        stepButton = new int[3];
        opacityButton = new int[]{250, 250, 250};
        rotationButton = new int[]{-15, 0, 15};
        rotation2Button = new int[]{90, 90, 90};
        opacity2Button = new int[]{0, 0, 0};
        stepBackButton = 0;
        playIsActivated = false;
        menuState = 0;
        stepHelpButton = 0;
        helpHoover = false;
    }


}
