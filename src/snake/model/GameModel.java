package snake.model;

import snake.control.timers.GameTimer;
import snake.model.exception.NotAPlayableSnake;
import snake.model.subgamemodel.*;
import snake.model.subgamemodel.solids.Fruits;
import snake.model.subgamemodel.solids.Tail;
import snake.model.subgamemodel.solids.Wall;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Classe de gestion du jeux.
 */
public class GameModel implements GameConstants, GlobalConstants {

    public final ArrayList<Fruits> fruits;
    public final WallLine[] wallLines;
    public final Snake[] players;
    public byte bonusvar;
    private final Random random;
    private boolean isPaused;
    private boolean isFinished;
    private boolean isLaunched;
    private Timer timer;
    private int alivePlayer;
    private boolean bonus;
    public Boolean hoover;

    private Font pauseFontSize;
    private Font pressFontSize;
    private Font menuFontSize;

    public final Music musicGame;

    public GameModel(int nJoueur) {
        hoover = false;
        bonusvar = 0;
        random = new Random();
        players = new Snake[nJoueur];
        alivePlayer = nJoueur;
        fruits = new ArrayList<Fruits>(3);
        wallLines = new WallLine[N_WALL_LINE_MIN + random.nextInt(N_WALL_LINE_MAX - N_WALL_LINE_MIN)];
        defineWalls();
        defineSnakes();
        for (int i = 0; i < (players.length + FRUITS_NUMBER_MODULATION); i++)
            summonFruits();
        isPaused = false;
        isLaunched = false;
        isFinished = false;
        timer = new Timer(FRAME_RATE, new GameTimer());
        bonus = false;

        musicGame = new Music("/assets/sound/snake.wav");

        try {
            Font mainFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Light.ttf"));
            pauseFontSize = mainFont.deriveFont(50f);
            menuFontSize = mainFont.deriveFont(35f);
            pressFontSize = mainFont.deriveFont(20f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isBonus() {
        return bonus;
    }

    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }

    public void setTimerState() {
        if (isLaunched)
            timer.start();
        else
            timer.stop();
    }

    public void setTimerState(boolean isLaunched) {
        if (isLaunched)
            timer.start();
        else
            timer.stop();
    }

    public boolean isLaunched() {
        return isLaunched;
    }

    public void setLaunched(boolean isLaunched) {
        this.isLaunched = isLaunched;
    }

    public void update() {
        if (!isPaused) {
            // Collision des joueurs
            for (Snake player : players) {
                player.update();
                for (Snake otherPlayer : players)
                    if (player.collideWith(otherPlayer))
                        killSnake(player, (player.equals(otherPlayer)) ? EndCause.SELF_EATING : EndCause.EAT_THE_ENEMIES);
                for (WallLine wallLine : wallLines)
                    for (Wall wall : wallLine.getWalls())
                        if (player.collideWith(wall))
                            killSnake(player, EndCause.SMASH_A_WALL);
                for (Fruits fruit : fruits)
                    player.collideWith(fruit);
            }

            // Peremption des fruits et création de nouveaux fruits si absence ou suppression de fruit
            ArrayList<Fruits> rmFruits = new ArrayList<Fruits>(1);
            for (Fruits fruit : fruits) {
                fruit.update();
                if (!fruit.isEatable())
                    rmFruits.add(fruit);
            }
            if (rmFruits.size() > 0 || fruits.size() == 0)
                summonFruits();
            for (Fruits fruit : rmFruits)
                fruits.remove(fruit);
        }
    }

    public void summonFruits() {
        int x = 0, y = 0;
        boolean regen = true;
        while (regen) {
            x = random.nextInt(WINDOW_WIDTH);
            y = random.nextInt(WINDOW_HEIGHT);
            regen = false;
            for (Fruits fruit : fruits)
                if (fruit.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_FRUITS))
                    regen = true;
            for (WallLine wallLine : wallLines)
                for (Wall wall : wallLine.getWalls())
                    if (wall.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_WALL))
                        regen = true;
            for (Snake player : players) {
                if (player.head.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_HEAD))
                    regen = true;
                for (Tail frag : player.tails)
                    if (frag.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_HEAD))
                        regen = true;
            }
        }
        byte calories = (byte) (MIN_CALORIES + random.nextInt(MAX_CALORIES - MIN_CALORIES));
        short duration = (short) (DURATION_MAX - (calories - MIN_CALORIES) * DURATION_FACTOR);
        this.fruits.add(new Fruits((short) x, (short) y, calories, duration));
    }

    public void defineWalls() {
        for (int i = 0; i < wallLines.length; i++)
            wallLines[i] = new WallLine(random);
    }

    public void defineSnakes() {
        for (int i = 0; i < players.length; i++) {
            int x = SNAKE_GENERATION_MARGIN, y = SNAKE_GENERATION_MARGIN;
            boolean regen = true;
            while (regen) {
                x = SNAKE_GENERATION_MARGIN + random.nextInt(WINDOW_WIDTH - SNAKE_GENERATION_MARGIN * 2);
                y = SNAKE_GENERATION_MARGIN + random.nextInt(WINDOW_HEIGHT - SNAKE_GENERATION_MARGIN * 2);
                regen = false;
                for (WallLine wallLine : wallLines)
                    for (Wall wall : wallLine.getWalls())
                        if (wall.collideWith(x, y, SNAKE_GENERATION_MARGIN_FOR_OTHER))
                            regen = true;
                for (int j = 0; j < i; j++)
                    if (players[j].head.collideWith(x, y, SNAKE_GENERATION_MARGIN_FOR_OTHER))
                        regen = true;
            }
            players[i] = new Snake((short) x, (short) y);
            players[i].head.setAngle((random.nextDouble() * 2 - 1) * Math.PI);
        }
    }

    public void killSnake(Snake loser, EndCause cause) {
        if (!loser.isDead()) {
            loser.selfScore *= cause.getScoreMultiplicant();
            saveScore(loser.getScore());
            loser.setDead(true);
            alivePlayer--;
            Thread die = new Sound("/assets/sound/death.wav");
            die.start();
            if (alivePlayer == 0) {
                isFinished = true;
                musicGame.stop();
                musicGame.beginning();
            }
        }
    }

    public Snake getPlayers(int n) throws NotAPlayableSnake {
        if (0 > n && n >= players.length)
            throw new NotAPlayableSnake(n);
        return players[n];
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }

    public boolean isEnd() {
        return isFinished;
    }

    public Snake[] getSnake() {
        return players;
    }

    public void dispose() {
        timer.stop();
        this.timer = null;
        int i;
        for (i = 0; i < wallLines.length; i++) {
            wallLines[i].dispose();
            wallLines[i] = null;
        }
        fruits.clear();
        for (i = 0; i < players.length; i++) {
            players[i].dispose();
            players[i] = null;
        }
        isPaused = isLaunched = true;
    }


    public void saveScore(int Score) {

        String fichier;
        if (players.length == 1)
            fichier = MainModel.path+"solo.txt";
        else
            fichier = MainModel.path+"multi.txt";
        int z = 0;
        int[] tabscore = new int[5];

        //lecture du fichier texte
        try {
            InputStream ips = new FileInputStream(fichier);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            while ((ligne = br.readLine()) != null) {
                int i = Integer.parseInt(ligne);
                tabscore[z] = i;
                z++;
            }
            br.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //création ou ajout dans le fichier texte
        try {
            FileWriter fw = new FileWriter(fichier);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter fichierSortie = new PrintWriter(bw);
            if (Score > tabscore[0] || Score == tabscore[0]) {
                tabscore[4] = tabscore[3];
                tabscore[3] = tabscore[2];
                tabscore[2] = tabscore[1];
                tabscore[1] = tabscore[0];
                tabscore[0] = Score;
            } else if (Score > tabscore[1] && Score < tabscore[0] || Score == tabscore[1]) {
                tabscore[4] = tabscore[3];
                tabscore[3] = tabscore[2];
                tabscore[2] = tabscore[1];
                tabscore[1] = Score;
            } else if (Score > tabscore[2] && Score < tabscore[1] || Score == tabscore[2]) {
                tabscore[4] = tabscore[3];
                tabscore[3] = tabscore[2];
                tabscore[2] = Score;
            } else if (Score > tabscore[3] && Score < tabscore[2] || Score == tabscore[3]) {
                tabscore[4] = tabscore[3];
                tabscore[3] = Score;
            } else if (Score > tabscore[4] && Score < tabscore[3] || Score == tabscore[4]) {
                tabscore[4] = Score;
            }
            fichierSortie.println(tabscore[0]);
            fichierSortie.println(tabscore[1]);
            fichierSortie.println(tabscore[2]);
            fichierSortie.println(tabscore[3]);
            fichierSortie.println(tabscore[4]);
            fichierSortie.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public Font getPauseFontSize() {
        return pauseFontSize;
    }

    public Font getPressFontSize() {
        return pressFontSize;
    }

    public Font getMenuFontSize() {
        return menuFontSize;
    }
}
