package snake.model;

import snake.MainSnake;
import snake.model.exception.NotCreatedScoreFile;

import java.io.*;

/**
 * Modele prinncipal
 */
public class MainModel {
    final static String path  = new File(MainSnake.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getPath()+"/";

    private GameModel game;
    private MenuModel menu;
    private LoseModel lose;
    private HighScoreModel highScore;
    private int nPlayer;


    public MainModel() {
        System.out.println(path);
        this.nPlayer = 1;
        this.menu = new MenuModel();
        this.lose = new LoseModel();
        this.highScore = new HighScoreModel();
    }

    public static int[] lireScore(String who) throws NotCreatedScoreFile {
        String fichier = path+who + ".txt";
        int[] tabscore = new int[5];

        //lecture du fichier texte
        try {
            InputStream ips = new FileInputStream(fichier);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            int z = 0;
            while ((ligne = br.readLine()) != null) {
                tabscore[z] = Integer.parseInt(ligne);
                z++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            throw new NotCreatedScoreFile(who);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tabscore;
    }

    public void newGame() {
        if (this.game != null)
            this.game.dispose();
        this.game = new GameModel(this.nPlayer);
        this.menu.musicMenu.stop();
        this.menu.musicMenu.beginning();
        this.game.musicGame.play();
    }

    public void newGame(int nPlayer) {
        this.nPlayer = nPlayer;
        newGame();
    }

    public GameModel getGame() {
        return game;
    }

    public MenuModel getMenu() {
        return menu;
    }

    public LoseModel getLose() {
        return lose;
    }

    public HighScoreModel getHighScore() {
        return highScore;
    }

    public static int newScore(int Score, MainModel model) { // Retourne le classement selon le score
        int[] scoreSolo;
        try {
            if (model.getGame().players.length == 1)
                scoreSolo = MainModel.lireScore(MainModel.path+"solo");
            else
                scoreSolo = MainModel.lireScore(MainModel.path+"multi");
        } catch (NotCreatedScoreFile e) {
            if (Score > 0) return 1;
            else return 0;
        }
        if (Score > 0) {
            if (Score > scoreSolo[0] || Score == scoreSolo[0]) {
                return 1;
            } else if (Score > scoreSolo[1] && Score < scoreSolo[0] || Score == scoreSolo[1]) {
                return 2;
            } else if (Score > scoreSolo[2] && Score < scoreSolo[1] || Score == scoreSolo[2]) {
                return 3;
            } else if (Score > scoreSolo[3] && Score < scoreSolo[2] || Score == scoreSolo[3]) {
                return 4;
            } else if (Score > scoreSolo[4] && Score < scoreSolo[3] || Score == scoreSolo[4]) {
                return 5;
            } else if (Score < scoreSolo[4]) {
                return 6;
            }
        }
        return 0;
    }
}




