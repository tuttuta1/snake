package snake.model;

import snake.model.subgamemodel.GameConstants;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Model de gestion quand on perd
 */

public class LoseModel implements GameConstants, GlobalConstants {

    public boolean animMasque;
    public byte n;
    public int stepMasqueLose;
    public int stepLine;
    public int[] score;
    public final int[] stepButton;
    public final boolean[] hoover;
    public int t;
    public Font scoreFontSize;
    public Font overFontSize;
    public Font buttonFontSize;
    public Font playerFontSize;
    public Font newScoreSize;

    public LoseModel() {
        n = 0;
        animMasque = false;
        stepMasqueLose = 0;
        stepLine = 0;
        score = new int[0];
        stepButton = new int[2];
        hoover = new boolean[2];
        t = 0;

        try {
            Font mainFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Light.ttf"));
            Font boldFont = Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("/assets/font/OpenSans-Bold.ttf"));
            overFontSize = boldFont.deriveFont(60f);
            buttonFontSize = mainFont.deriveFont(35f);
            scoreFontSize = mainFont.deriveFont(70f);
            playerFontSize = boldFont.deriveFont(20f);
            newScoreSize = boldFont.deriveFont(25f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void initStepButton() {
        t = stepButton[0] = stepButton[1] = 0;
    }


}

