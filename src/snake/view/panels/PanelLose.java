package snake.view.panels;

import snake.control.timers.LoseTimer;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.model.subgamemodel.Snake;
import snake.view.MainWindow;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

/**
 * Panel de fin de jeux
 */
public class PanelLose extends JPanel implements GlobalConstants, ViewConstants {

    private final MainModel model;
    private final MainWindow window;
    public final JButton newGame;
    private final JButton exit;
    private final Color colorNewGame = new Color(0, 0, 0);
    private final Color colorExit = new Color(0, 0, 0);

    public PanelLose(MainModel model, MainWindow window) {
        this.window = window;
        this.model = model;
        newGame = new JButton();
        exit = new JButton();
        Timer timer = new Timer(FRAME_RATE, new LoseTimer());
        timer.start();

        newGame.setContentAreaFilled(false);
        newGame.setFocusPainted(false);
        newGame.setMargin(null);
        newGame.setBorder(BorderFactory.createEmptyBorder());
        exit.setContentAreaFilled(false);
        exit.setFocusPainted(false);
        exit.setMargin(null);
        exit.setBorder(BorderFactory.createEmptyBorder());
        this.add(newGame);
        this.add(exit);

        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        FontMetrics metrics = g2.getFontMetrics(model.getLose().scoreFontSize);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(window.getGamePanel().previousPanel);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        g2.setColor(Color.black);

        int adv;
        if (model.getGame().players.length == 2) {
            g2.setFont(model.getLose().overFontSize);
            g2.drawString("Game Over", WINDOW_WIDTH / 4 + 10, 180);

            g2.setColor(Color.black);
            g2.setColor(new Color(0, 0, 0, 150));
            g2.setFont(model.getLose().newScoreSize);

            if (MainModel.newScore(model.getLose().score[0], model) > 0 &&
                    MainModel.newScore(model.getLose().score[0], model) < 6) {
                adv = metrics.stringWidth("" + model.getLose().score[0]);
                g2.drawString("New Highscore ! Rank : " + MainModel.newScore(model.getLose().score[0], model),
                        WINDOW_WIDTH / 4 + 100 + adv, WINDOW_HEIGHT / 2 - 30);
            }

            if (MainModel.newScore(model.getLose().score[1], model) > 0 &&
                    MainModel.newScore(model.getLose().score[1], model) < 6) {
                adv = metrics.stringWidth("" + model.getLose().score[1]);
                g2.drawString("New Highscore ! Rank : " + MainModel.newScore(model.getLose().score[1], model),
                        WINDOW_WIDTH / 4 + +100 + adv, WINDOW_HEIGHT / 2 + 40);
            }

            g2.setFont(model.getLose().playerFontSize);
            g2.drawString("P1", WINDOW_WIDTH / 4 + 10, WINDOW_HEIGHT / 2 - 30);
            g2.drawString("P2", WINDOW_WIDTH / 4 + 10, WINDOW_HEIGHT / 2 + 40);
            g2.setFont(model.getLose().scoreFontSize);
            g2.drawString("" + model.getLose().score[0], WINDOW_WIDTH / 4 + 40, WINDOW_HEIGHT / 2 - 30);
            g2.drawString("" + model.getLose().score[1], WINDOW_WIDTH / 4 + 40, WINDOW_HEIGHT / 2 + 40);


            g2.setFont(model.getLose().buttonFontSize);
            g2.setColor(colorNewGame);
            g2.drawString("New Game", WINDOW_WIDTH / 4 + 10 + model.getLose().stepButton[0], 380);
            g2.setColor(colorExit);
            g2.drawString("Menu", WINDOW_WIDTH / 4 + 10 + model.getLose().stepButton[1], 430);

            newGame.setBounds(WINDOW_WIDTH / 4 + 10, 340, 225, 50);
            exit.setBounds(WINDOW_WIDTH / 4 + 10, 390, 150, 50);

        } else {
            g2.setFont(model.getLose().overFontSize);
            g2.drawString("Game Over", WINDOW_WIDTH / 4 + 10, 210);
            g2.setColor(new Color(0, 0, 0, 150));
            g2.setFont(model.getLose().newScoreSize);


            if (MainModel.newScore(model.getLose().score[0], model) > 0 &&
                    MainModel.newScore(model.getLose().score[0], model) < 6) {
                metrics = g2.getFontMetrics(model.getLose().scoreFontSize);
                adv = metrics.stringWidth("" + model.getLose().score[0]);
                g2.drawString("New Highscore ! Rank : " + MainModel.newScore(model.getLose().score[0], model), WINDOW_WIDTH / 4 + adv + 100, WINDOW_HEIGHT / 2);
            }

            g2.setFont(model.getLose().playerFontSize);
            g2.drawString("P1", WINDOW_WIDTH / 4 + 10, WINDOW_HEIGHT / 2);
            g2.setFont(model.getLose().scoreFontSize);
            g2.drawString("" + model.getLose().score[0], WINDOW_WIDTH / 4 + 40, WINDOW_HEIGHT / 2);

            g2.setFont(model.getLose().buttonFontSize);
            g2.setColor(colorNewGame);
            g2.drawString("New Game", WINDOW_WIDTH / 4 + 10 + model.getLose().stepButton[0], 340);
            g2.setColor(colorExit);
            g2.drawString("Menu", WINDOW_WIDTH / 4 + 10 + model.getLose().stepButton[1], 390);
            newGame.setBounds(WINDOW_WIDTH / 4 + 10, 300, 225, 50);
            exit.setBounds(WINDOW_WIDTH / 4 + 10, 350, 150, 50);
        }


        g2.setColor(Color.black);
        g2.drawLine(WINDOW_WIDTH / 4 + model.getLose().stepMasqueLose,
                WINDOW_HEIGHT / 6 + model.getLose().stepLine,
                WINDOW_WIDTH / 4 + model.getLose().stepMasqueLose,
                WINDOW_HEIGHT - WINDOW_HEIGHT / 6 - model.getLose().stepLine);
        g2.setColor(window.getGamePanel().previousPanel);
        g2.fillRect(0, WINDOW_HEIGHT / 6,
                WINDOW_WIDTH / 4 + model.getLose().stepMasqueLose,
                WINDOW_HEIGHT - WINDOW_HEIGHT / 6 - WINDOW_HEIGHT / 6);

    }

    public double easeIn(int x) {
        return x * x * 0.01;
    }

    public void update() {
        if (!model.getLose().animMasque) {
            for (byte i = 0; i < 2; i++) {
                if (model.getLose().hoover[i]) {
                    if (model.getLose().stepButton[i] < MAX_POS)
                        model.getLose().stepButton[i] += SPEED_STEP_BUTTON;
                    else if (model.getLose().stepButton[i] != MAX_POS)
                        model.getLose().stepButton[i] = MAX_POS;
                } else {
                    if (model.getLose().stepButton[i] > MIN_POS)
                        model.getLose().stepButton[i] -= SPEED_STEP_BUTTON;
                    else if (model.getLose().stepButton[i] != MIN_POS)
                        model.getLose().stepButton[i] = MIN_POS;
                }
            }
        } else {
            if (model.getLose().stepMasqueLose <= WINDOW_WIDTH) {
                model.getLose().stepMasqueLose += easeIn(model.getLose().t);
                model.getLose().t++;
            }
            if (model.getLose().stepMasqueLose > WINDOW_WIDTH) {
                switch (model.getLose().n) {
                    case 1:
                        model.newGame();
                        window.getGamePanel().updateModel(model);
                        model.getLose().initStepButton();
                        setHoover(0, false);

                        window.switchPanel(window.getGamePanel());
                        break;

                    case 2:
                        model.getLose().initStepButton();
                        setHoover(0, false);

                        model.getMenu().setTimerState(true);
                        model.getMenu().reset();
                        window.switchPanel(window.getMenuPanel());
                        break;
                }
                model.getLose().animMasque = false;
                model.getLose().stepLine = model.getLose().stepMasqueLose = 0;
            }
        }
    }


    public void setMouseListener(MouseListener listener) {
        newGame.addMouseListener(listener);
        exit.addMouseListener(listener);
    }

    public void setActionListener(ActionListener listener) {
        newGame.addActionListener(listener);
        exit.addActionListener(listener);
    }

    public void updateScore() {
        Snake[] list = model.getGame().players;
        model.getLose().score = new int[list.length];
        for (byte i = 0; i < list.length; i++)
            model.getLose().score[i] = list[i].getScore();
    }

    public void setHoover(int button, boolean b) {
        model.getLose().hoover[button] = b;
    }

}


