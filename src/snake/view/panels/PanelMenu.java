package snake.view.panels;

import snake.control.ControlMenu;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Class pour la generation du menu principal
 */

public class PanelMenu extends JPanel implements GlobalConstants, ViewConstants {

    public final JButton button1;
    public final JButton button2;
    public final JButton button3;
    public final JButton helpButton;
    private final MainModel model;
    private Graphics2D g2;

    private AffineTransform orig;

    private Color color = GAME_COLOR;

    public PanelMenu(MainModel model) {
        this.model = model;
        button1 = new JButton();
        button1.setBounds(WINDOW_WIDTH / 2 + 165, WINDOW_HEIGHT / 2 - 100, 170, 75);
        button1.setContentAreaFilled(false);
        button1.setFocusPainted(false);
        button1.setMargin(null);
        button1.setBorder(BorderFactory.createEmptyBorder());
        button1.setActionCommand("1");
        this.add(button1);

        button2 = new JButton();
        button2.setBounds(WINDOW_WIDTH / 2 + 165, WINDOW_HEIGHT / 2 - 20, 105, 50);
        button2.setContentAreaFilled(false);
        button2.setFocusPainted(false);
        button2.setMargin(null);
        button2.setBorder(BorderFactory.createEmptyBorder());
        button2.setActionCommand("2");
        this.add(button2);

        button3 = new JButton();
        button3.setBounds(WINDOW_WIDTH / 2 + 165, WINDOW_HEIGHT / 2 + 35, 80, 50);
        button3.setContentAreaFilled(false);
        button3.setFocusPainted(false);
        button3.setMargin(null);
        button3.setBorder(BorderFactory.createEmptyBorder());
        button3.setActionCommand("3");
        this.add(button3);

        helpButton = new JButton();
        helpButton.setBounds(WINDOW_WIDTH - 75, 25, 50, 50);
        helpButton.setContentAreaFilled(false);
        helpButton.setFocusPainted(false);
        helpButton.setMargin(null);
        helpButton.setBorder(BorderFactory.createEmptyBorder());
        this.add(helpButton);

        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
    }

    private void drawLogoMenu(String s, int XPos, int YPos) {
        FontMetrics fm = g2.getFontMetrics();
        java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g2);
        int textHeight = (int) (rect.getHeight());
        int textWith = (int) (rect.getWidth());
        int x = XPos - textWith / 2;
        int y = YPos + textHeight / 8;

        g2.drawString(s, x, y + ((fm.getDescent() + fm.getAscent()) / 2));
    }


    @Override
    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        orig = g2.getTransform();

        g2.setColor(color);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        g2.setColor(Color.black);
        g2.drawOval(WINDOW_WIDTHd2 - MAIN_CIRCLE_RAY, WINDOW_HEIGHTd2 - MAIN_CIRCLE_RAY, MAIN_CIRCLE_DIAMETER, MAIN_CIRCLE_DIAMETER);

        final int menuState = model.getMenu().getMenuState();

        g2.setFont(model.getMenu().getSubMenuSize());
        drawMenuLine(0, menuState);
        g2.setFont(menuState != 0 ? model.getMenu().getSubMenuSize() : model.getMenu().getPlayFontSize());
        drawMenuLine(1, menuState);
        g2.setFont(model.getMenu().getSubMenuSize());
        drawMenuLine(2, menuState);
        drawStringSnake();

        drawButtonHelp();

    }

    public void drawStringCenter(String s, int x, int y) {
        FontMetrics fm = g2.getFontMetrics();

        g2.drawString(s, x - (fm.stringWidth(s) / 2), y + (fm.getDescent() + fm.getAscent()) / 4);
    }

    public void drawMenuLine(int n, int stateMenu) {
        FontMetrics fm = g2.getFontMetrics();
        java.awt.geom.Rectangle2D rect = fm.getStringBounds(TEXT_MENU[stateMenu][n], g2);
        int y = (int) (WINDOW_HEIGHTd2 - rect.getHeight() / 4);
        if (stateMenu == 1) {
            g2.setColor(new Color(0, 0, 0, model.getMenu().opacity2Button[n]));
            g2.rotate(Math.toRadians(model.getMenu().rotation2Button[n]), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        } else {
            g2.setColor(new Color(0, 0, 0, model.getMenu().opacityButton[n]));
            g2.rotate(Math.toRadians(model.getMenu().rotationButton[n]), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        }
        g2.drawString(TEXT_MENU[stateMenu][n], WINDOW_WIDTHd2 + 170 + model.getMenu().stepButton[n], y + ((fm.getDescent() + fm.getAscent()) / 2));
        g2.setTransform(orig);
    }

    public void drawButtonHelp() {
        g2.setFont(model.getMenu().getHelpFontSize().deriveFont(model.getMenu().getHelpFontSize().getSize() + model.getMenu().stepHelpButton));
        drawStringCenter("?", WINDOW_WIDTH - 50, 50);
    }

    public void drawStringSnake() {
        g2.setFont(model.getMenu().getLogoMenuSize());
        g2.setColor(new Color(0, 0, 0));

        g2.rotate(RAY_LOGO_A, WINDOW_WIDTHd2, WINDOW_HEIGHTd2);
        g2.rotate(RAY_LOGO_B, WINDOW_WIDTHd2 + 155, WINDOW_HEIGHTd2);
        drawLogoMenu("S", WINDOW_WIDTHd2 + 150, WINDOW_HEIGHTd2);

        g2.setTransform(orig);

        g2.rotate(RAY_LOGO_A + RAY_LOGO_BETWEEN_LETTERS, WINDOW_WIDTHd2, WINDOW_HEIGHTd2);
        g2.rotate(RAY_LOGO_B, WINDOW_WIDTHd2 + 155, WINDOW_HEIGHTd2);
        drawLogoMenu("N", WINDOW_WIDTHd2 + 150, WINDOW_HEIGHTd2);
        g2.setTransform(orig);

        g2.rotate(RAY_LOGO_A + RAY_LOGO_BETWEEN_LETTERS * 2, WINDOW_WIDTHd2, WINDOW_HEIGHTd2);
        g2.rotate(RAY_LOGO_B, WINDOW_WIDTHd2 + 155, WINDOW_HEIGHTd2);
        drawLogoMenu("A", WINDOW_WIDTHd2 + 150, WINDOW_HEIGHTd2);
        g2.setTransform(orig);

        g2.rotate(RAY_LOGO_A + RAY_LOGO_BETWEEN_LETTERS * 3, WINDOW_WIDTHd2, WINDOW_HEIGHTd2);
        g2.rotate(RAY_LOGO_B, WINDOW_WIDTHd2 + 155, WINDOW_HEIGHTd2);

        drawLogoMenu("K", WINDOW_WIDTHd2 + 150, WINDOW_HEIGHTd2);
        g2.setTransform(orig);

        g2.rotate(RAY_LOGO_A + RAY_LOGO_BETWEEN_LETTERS * 4, WINDOW_WIDTHd2, WINDOW_HEIGHTd2);
        g2.rotate(RAY_LOGO_B, WINDOW_WIDTHd2 + 155, WINDOW_HEIGHTd2);
        drawLogoMenu("E", WINDOW_WIDTHd2 + 150, WINDOW_HEIGHTd2);
        g2.setTransform(orig);
    }


    public void setMenuControler(ControlMenu listener) {
        button1.addMouseListener(listener);
        button2.addMouseListener(listener);
        button3.addMouseListener(listener);
        helpButton.addMouseListener(listener);
        button1.addActionListener(listener);
        button2.addActionListener(listener);
        button3.addActionListener(listener);
        helpButton.addActionListener(listener);
    }

    public void update() {
        if (model.getMenu().getAnimationDuring()) {
            if (model.getMenu().getPlayIsActivated()) {
                if (model.getMenu().opacity2Button[0] > 0) {
                    model.getMenu().opacity2Button[0] -= SPEED_OPACITY;
                    model.getMenu().opacity2Button[1] -= SPEED_OPACITY;
                }
                if (model.getMenu().opacity2Button[0] <= 0) {
                    model.getMenu().opacity2Button[0] = 0;
                    model.getMenu().opacity2Button[1] = 0;
                    if (model.getMenu().rotation2Button[2] < 90) {
                        model.getMenu().rotation2Button[2] += SPEED;
                        model.getMenu().opacity2Button[2] -= SPEED_OPACITY + 2;
                    }
                    if (model.getMenu().rotation2Button[2] >= 90) {
                        model.getMenu().opacity2Button[2] = 0;
                        model.getMenu().rotation2Button[0] = model.getMenu().rotation2Button[1] = model.getMenu().rotation2Button[2] = 90;
                        model.getMenu().stepButton[0] = model.getMenu().stepButton[1] = model.getMenu().stepButton[2] = 0;
                        model.getMenu().setMenuState(0);
                        if (model.getMenu().rotationButton[1] < 0) {
                            model.getMenu().rotationButton[1] += SPEED;
                            model.getMenu().opacityButton[1] += SPEED_OPACITY / 2;
                        }
                        if (model.getMenu().rotationButton[1] >= 0) {
                            model.getMenu().rotationButton[1] = 0;
                            model.getMenu().opacityButton[1] = 250;
                            if (model.getMenu().opacityButton[0] < 250) {
                                model.getMenu().opacityButton[0] += SPEED_OPACITY / 2;
                                model.getMenu().opacityButton[2] += SPEED_OPACITY / 2;
                            }
                            if (model.getMenu().opacityButton[0] >= 250) {
                                model.getMenu().opacityButton[0] = 250;
                                model.getMenu().opacityButton[2] = 250;
                                model.getMenu().setAnimationDuring(false);
                                model.getMenu().setPlayIsActivated(false);
                            }

                        }
                    }
                }
            } else {
                if (model.getMenu().opacityButton[0] > 0) {
                    model.getMenu().opacityButton[0] -= SPEED_OPACITY;
                    model.getMenu().opacityButton[2] -= SPEED_OPACITY;
                }
                if (model.getMenu().opacityButton[0] <= 0) {
                    model.getMenu().opacityButton[0] = model.getMenu().opacityButton[2] = 0;
                    if (model.getMenu().opacityButton[1] > 0) {
                        model.getMenu().opacityButton[1] -= SPEED_OPACITY;
                        model.getMenu().rotationButton[1] -= SPEED;
                    }
                    if (model.getMenu().opacityButton[1] <= 0) {
                        model.getMenu().opacityButton[1] = 0;
                        model.getMenu().setMenuState(1);
                        model.getMenu().stepButton[0] = model.getMenu().stepButton[1] = model.getMenu().stepButton[2] = 0;
                        model.getMenu().rotationButton[1] = -90;
                        if (model.getMenu().rotation2Button[0] > -15) {
                            model.getMenu().opacity2Button[0] += 2;
                            model.getMenu().rotation2Button[0] -= 4;
                        }
                        if (model.getMenu().rotation2Button[0] <= -15) {
                            model.getMenu().opacity2Button[0] = 250;
                            model.getMenu().rotation2Button[0] = -15;
                            if (model.getMenu().rotation2Button[1] > 0) {
                                model.getMenu().opacity2Button[1] += 2;
                                model.getMenu().rotation2Button[1] -= 4;
                            }
                            if (model.getMenu().rotation2Button[1] <= 0) {
                                model.getMenu().opacity2Button[1] = 250;
                                model.getMenu().rotation2Button[1] = 0;
                                if (model.getMenu().rotation2Button[2] > 15) {
                                    model.getMenu().opacity2Button[2] += 2;
                                    model.getMenu().rotation2Button[2] -= 4;
                                }
                                if (model.getMenu().rotation2Button[2] <= 15) {
                                    model.getMenu().opacity2Button[2] = 250;
                                    model.getMenu().rotation2Button[2] = 15;
                                    model.getMenu().setAnimationDuring(false);
                                    model.getMenu().setPlayIsActivated(true);

                                }
                            }
                        }
                    }
                }
            }
        } else {
            for (byte i = 0; i < 3; i++) {
                if (model.getMenu().hoover[i]) {
                    if (model.getMenu().stepButton[i] < MAX_POS)
                        model.getMenu().stepButton[i] += SPEED;
                    else if (model.getMenu().stepButton[i] != MAX_POS)
                        model.getMenu().stepButton[i] = MAX_POS;
                } else {
                    if (model.getMenu().stepButton[i] > MIN_POS)
                        model.getMenu().stepButton[i] -= SPEED;
                    else if (model.getMenu().stepButton[i] != MIN_POS)
                        model.getMenu().stepButton[i] = MIN_POS;
                }
                if (model.getMenu().backHighscoreHoover) {
                    if (model.getMenu().stepBackButton < 10) {
                        model.getMenu().stepBackButton += 0.5;
                    } else if (model.getMenu().stepBackButton != 10) {
                        model.getMenu().stepBackButton = MIN_POS;
                        model.getMenu().backHighscoreHoover = false;
                    }

                } else {
                    if (model.getMenu().stepBackButton > 0) {
                        model.getMenu().stepBackButton -= 0.5;
                    }
                }
            }

        }
        if (model.getMenu().helpHoover) {
            if (model.getMenu().stepHelpButton < 10) {
                model.getMenu().stepHelpButton += 2;
            } else if (model.getMenu().stepHelpButton != 10) {
                model.getMenu().helpHoover = false;
            }
        } else {
            if (model.getMenu().stepHelpButton > 0) {
                model.getMenu().stepHelpButton -= 2;
            } else if (model.getMenu().stepHelpButton != 0) {
                model.getMenu().stepHelpButton -= 0.5;
            }
        }
    }

    public void setHoover(int button, boolean b) {
        model.getMenu().hoover[button] = b;
    }

    public void setHelpHoover(boolean b) {
        model.getMenu().helpHoover = b;
    }

    public void initStepButton() {
        model.getMenu().stepButton[0] = model.getMenu().stepButton[1] = model.getMenu().stepButton[2] = 0;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}