package snake.view.panels;

import snake.control.ControlHelp;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.view.ViewConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Panel de touches
 */
public class PanelHelp extends JPanel implements GlobalConstants, ViewConstants {

    public Color color;

    public final JButton backButton;

    private static BufferedImage imageHelp;

    private final MainModel model;

    private Graphics2D g2;

    public PanelHelp(MainModel model) {
        color = Color.black;

        try {
            imageHelp = ImageIO.read(PanelHelp.class.getResource("/assets/img/help.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        this.model = model;

        backButton = new JButton();
        backButton.setBounds(WINDOW_WIDTH - 175, WINDOW_HEIGHT - 65, 150, 50);
        backButton.setContentAreaFilled(false);
        backButton.setBorder(BorderFactory.createEmptyBorder());
        this.add(backButton);

        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
    }

    public void drawStringCenter(String s, int x, int y) {
        FontMetrics fm = g2.getFontMetrics();

        g2.drawString(s, x - (fm.stringWidth(s) / 2), y + (fm.getDescent() + fm.getAscent()) / 4);
    }


    @Override
    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(color);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        g2.drawImage(imageHelp, 0, 0, this);

        g2.setColor(Color.black);
        g2.setFont(model.getHighScore().getBackButtonSize().deriveFont(36f + model.getMenu().stepBackButton));
        drawStringCenter("Back", WINDOW_WIDTH - 100, WINDOW_HEIGHT - 40);
    }

    public void setColor(Color c) {
        color = c;
    }

    public void setHelpControler(ControlHelp listener) {
        backButton.addMouseListener(listener);
        backButton.addActionListener(listener);
    }
}
