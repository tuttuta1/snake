package snake.view.panels;

import snake.control.ControlHigh;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.model.exception.NotCreatedScoreFile;
import snake.model.subgamemodel.GameConstants;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Panel highScore
 */

public class PanelHighScore extends JPanel implements GameConstants, ViewConstants, GlobalConstants {

    private final MainModel model;

    private Graphics2D g2;

    public final JButton backbutton;
    public Color color;

    private AffineTransform orig;

    public PanelHighScore(MainModel model) {
        this.model = model;
        backbutton = new JButton();
        color = GAME_COLOR;

        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        backbutton.setBounds(WINDOW_WIDTH - 175, WINDOW_HEIGHT - 65, 150, 50);
        backbutton.setContentAreaFilled(false);
        backbutton.setBorder(BorderFactory.createEmptyBorder());
        this.add(backbutton);
    }


    @Override
    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(color);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        g2.setFont(model.getHighScore().getScoresFontSize());
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(model.getHighScore().getTitreScoreSize());
        orig = g2.getTransform();
        g2.rotate(Math.toRadians(-90), WINDOW_WIDTH / 2 - 25, 490);
        drawStringLine("Scores Solo", WINDOW_WIDTH / 2 - 25, 490);
        g2.rotate(Math.toRadians(90), WINDOW_WIDTH / 2 + 25, 110);
        drawStringLine("Scores Multi", WINDOW_WIDTH / 2 + 25, 110);
        g2.setTransform(orig);
        g2.setFont(model.getHighScore().getBackButtonSize().deriveFont(36f + model.getMenu().stepBackButton));
        drawStringCenter("Back", WINDOW_WIDTH - 100, WINDOW_HEIGHT - 40);
        g2.setFont(model.getHighScore().getScoresFontSize());
        scoreSolo();
        scoreMulti();
    }

    public void drawStringCenter(String s, int x, int y) {
        FontMetrics fm = g2.getFontMetrics();

        g2.drawString(s, x - (fm.stringWidth(s) / 2), y + (fm.getDescent() + fm.getAscent()) / 4);
    }

    public void drawStringLine(String s, int x, int yf) {
        FontMetrics fm = g2.getFontMetrics();

        g2.drawString(s, x, yf + (fm.getDescent() + fm.getAscent()) / 4);
        g2.setTransform(orig);
    }

    private void scoreMulti() {
        g2.setFont(model.getHighScore().getScoresFontSize());
        try {
            model.getHighScore().scoreMulti();
            g2.drawString(Integer.toString(model.getHighScore().scoresMulti[0]), WINDOW_WIDTH / 2 + 250, WINDOW_HEIGHT / 2 - 120);
            g2.drawString(Integer.toString(model.getHighScore().scoresMulti[1]), WINDOW_WIDTH / 2 + 250, WINDOW_HEIGHT / 2 - 50);
            g2.drawString(Integer.toString(model.getHighScore().scoresMulti[2]), WINDOW_WIDTH / 2 + 250, WINDOW_HEIGHT / 2 + 20);
            g2.drawString(Integer.toString(model.getHighScore().scoresMulti[3]), WINDOW_WIDTH / 2 + 250, WINDOW_HEIGHT / 2 + 90);
            g2.drawString(Integer.toString(model.getHighScore().scoresMulti[4]), WINDOW_WIDTH / 2 + 250, WINDOW_HEIGHT / 2 + 160);
        } catch (NotCreatedScoreFile e) {
            g2.drawString("Il n'y aucun score Multi", 10, 10);
        }

        g2.setFont(model.getHighScore().getScoresFontSize2());
        g2.drawString("1.", WINDOW_WIDTH / 2 + 220, WINDOW_HEIGHT / 2 - 120);
        g2.drawString("2.", WINDOW_WIDTH / 2 + 220, WINDOW_HEIGHT / 2 - 50);
        g2.drawString("3.", WINDOW_WIDTH / 2 + 220, WINDOW_HEIGHT / 2 + 20);
        g2.drawString("4.", WINDOW_WIDTH / 2 + 220, WINDOW_HEIGHT / 2 + 90);
        g2.drawString("5.", WINDOW_WIDTH / 2 + 220, WINDOW_HEIGHT / 2 + 160);


    }

    private void scoreSolo() {
        int decalage = 200;
        try {
            model.getHighScore().scoreSolo();
            g2.drawString(Integer.toString(model.getHighScore().scoresSolo[0]), decalage, WINDOW_HEIGHT / 2 - 120);
            g2.drawString(Integer.toString(model.getHighScore().scoresSolo[1]), decalage, WINDOW_HEIGHT / 2 - 50);
            g2.drawString(Integer.toString(model.getHighScore().scoresSolo[2]), decalage, WINDOW_HEIGHT / 2 + 20);
            g2.drawString(Integer.toString(model.getHighScore().scoresSolo[3]), decalage, WINDOW_HEIGHT / 2 + 90);
            g2.drawString(Integer.toString(model.getHighScore().scoresSolo[4]), decalage, WINDOW_HEIGHT / 2 + 160);
        } catch (NotCreatedScoreFile e) {
            g2.drawString("Il n'y aucun score solo", 10, 10);

        }

        g2.setFont(model.getHighScore().getScoresFontSize2());
        g2.drawString("1.", decalage - 30, WINDOW_HEIGHT / 2 - 120);
        g2.drawString("2.", decalage - 30, WINDOW_HEIGHT / 2 - 50);
        g2.drawString("3.", decalage - 30, WINDOW_HEIGHT / 2 + 20);
        g2.drawString("4.", decalage - 30, WINDOW_HEIGHT / 2 + 90);
        g2.drawString("5.", decalage - 30, WINDOW_HEIGHT / 2 + 160);

        g2.drawLine(WINDOW_WIDTH / 2 + 1, 100, WINDOW_WIDTH / 2 + 1, 500);
        g2.drawLine(WINDOW_WIDTH / 2 + 1, 100, WINDOW_WIDTH / 2 + 1, 500);
    }


    public void setHighControler(ControlHigh listener) {
        backbutton.addMouseListener(listener);
        backbutton.addActionListener(listener);
    }

    public void setColor(Color c) {
        color = c;
    }
}
