package snake.view.panels;

import snake.control.ControlGame;
import snake.model.GameModel;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.model.subgamemodel.GameConstants;
import snake.model.subgamemodel.Snake;
import snake.model.subgamemodel.WallLine;
import snake.model.subgamemodel.solids.Fruits;
import snake.model.subgamemodel.solids.Tail;
import snake.model.subgamemodel.solids.Wall;
import snake.view.ViewConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Panel de jeux
 */
public class PanelGame extends JPanel implements GameConstants, ViewConstants, GlobalConstants {

    private final static ArrayList<BufferedImage> fruits = new ArrayList<BufferedImage>(4);
    private static BufferedImage image_up_key;

    private Graphics2D g2;
    public Color previousPanel;
    public Color colorMenu;
    private GameModel model;
    private final Random rand;

    public final JButton menu_button;

    public PanelGame(MainModel model) {
        rand = new Random();
        this.model = model.getGame();
        colorMenu = new Color(250, 250, 250, 150);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        initImages();

        menu_button = new JButton();
        menu_button.setBounds(25, 130, 150, 40);
        menu_button.setContentAreaFilled(false);
        menu_button.setFocusPainted(false);
        menu_button.setMargin(null);
        menu_button.setVisible(false);
        menu_button.setBorder(BorderFactory.createEmptyBorder());
        this.add(menu_button);
    }

    public void updateModel(MainModel model) {
        this.model = model.getGame();
    }

    @Override
    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(previousPanel);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        if (model.isPaused()) {
            drawTail();
            drawHead();
            drawFruit();
            drawWall();
            g2.setColor(PAUSE_BLACK);
            g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
            g2.setColor(Color.WHITE);
            g2.setFont(model.getPauseFontSize());
            g2.drawString("Pause", 20, 60);
            g2.setFont(model.getPressFontSize());
            g2.drawString("press", 25, 100);
            g2.drawString("key to continue", 125, 100);
            g2.drawImage(image_up_key, 85, 80, this);
            g2.setFont(model.getMenuFontSize());
            g2.setColor(colorMenu);
            g2.drawString("Menu", 25, 160);

        } else {
            drawTail();
            drawHead();
            drawFruit();
            drawWall();
            g2.setColor(HOVER_COLOR);
            g2.fillRect(0, 0, WINDOW_WIDTH, 25);
            g2.setColor(Color.BLACK);
            g2.setFont(model.getPressFontSize());
            g2.drawString("J1 : "+model.players[0].selfScore, 10, 20);
            if (model.players.length>1)
                g2.drawString("| J2 : "+model.players[1].selfScore, WINDOW_WIDTHd2, 20);
        }

    }

    public void drawFruit() {
        for (Fruits frut : model.fruits) {
            if (frut.isEatable()) {
                if (frut.calories <= 8) {
                    g2.drawImage(fruits.get(0), frut.getX() - 15, frut.getY() - 15, this);
                } else if (frut.calories <= 12) {
                    g2.drawImage(fruits.get(1), frut.getX() - 15, frut.getY() - 15, this);
                } else if (frut.calories <= 16) {
                    g2.drawImage(fruits.get(2), frut.getX() - 15, frut.getY() - 15, this);
                } else if (frut.calories <= 21) {
                    g2.drawImage(fruits.get(3), frut.getX() - 15, frut.getY() - 15, this);
                }
            }
        }
    }

    public void drawTail() {
        byte playerNumber = 0;
        for (Snake player : model.players) {
            for (Tail frag : player.tails) {
                if (player.isDead()) g2.setColor(Color.gray);
                else if (model.isBonus())
                    g2.setColor(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
                else g2.setColor(TAIL_COLOR[playerNumber]);
                g2.fillOval(frag.getX() - frag.getRay(), frag.getY() - frag.getRay(), frag.getDiameter(), frag.getDiameter());
                if (!player.isDead() && !model.isBonus()) g2.setColor(HEAD_COLOR[playerNumber]);
                else g2.setColor(Color.black);
                g2.drawOval(frag.getX() - frag.getRay(), frag.getY() - frag.getRay(), frag.getDiameter(), frag.getDiameter());

            }
            playerNumber++;
        }
    }

    public void drawHead() {
        byte playerNumber = 0;
        for (Snake player : model.players) {
            int x = player.head.getX() - HEAD_RAY;
            int y = player.head.getY() - HEAD_RAY;
            double a = player.head.getAngle();
            if (model.isBonus() && !player.isDead())
                g2.setColor(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
            else if (!player.isDead()) g2.setColor(HEAD_COLOR[playerNumber]);
            else g2.setColor(Color.gray);
            g2.fillOval(x, y, HEAD_DIAMETER, HEAD_DIAMETER);

            g2.setColor(Color.black);
            g2.drawOval(x, y, HEAD_DIAMETER, HEAD_DIAMETER);
            if (player.isDead()) g2.setColor(Color.white);
            g2.fillOval((int) (player.head.getX() + Math.cos(a + ANGLE_EYES) * ECART_EYES) - RAY_EYES,
                    (int) (player.head.getY() + Math.sin(a + ANGLE_EYES) * ECART_EYES) - RAY_EYES, DIAMETER_EYES, DIAMETER_EYES);
            g2.fillOval((int) (player.head.getX() + Math.cos(a - ANGLE_EYES) * ECART_EYES) - RAY_EYES,
                    (int) (player.head.getY() + Math.sin(a - ANGLE_EYES) * ECART_EYES) - RAY_EYES, DIAMETER_EYES, DIAMETER_EYES);
            playerNumber++;
        }
    }

    public void drawWall() {
        for (WallLine wallLine : model.wallLines)
            for (Wall wall : wallLine.getWalls()) {
                g2.setColor(WALL_COLOR);
                g2.fillOval(wall.getX() - RAY_WALL, wall.getY() - RAY_WALL, WALL_DIAMETER, WALL_DIAMETER);
            }
    }

    public void setKeyControler(ControlGame listener) {
        menu_button.addActionListener(listener);
        menu_button.addMouseListener(listener);
        addKeyListener(listener);
    }


    public void initImages() {
        try {
            image_up_key = ImageIO.read(PanelGame.class.getResource("/assets/img/up_key.png"));
            fruits.add(ImageIO.read(PanelGame.class.getResource("/assets/img/grapefruit.png")));
            fruits.add(ImageIO.read(PanelGame.class.getResource("/assets/img/banana.png")));
            fruits.add(ImageIO.read(PanelGame.class.getResource("/assets/img/lollipop.png")));
            fruits.add(ImageIO.read(PanelGame.class.getResource("/assets/img/burger.png")));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
