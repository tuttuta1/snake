package snake.view;

import snake.model.MainModel;
import snake.model.subgamemodel.GameConstants;
import snake.view.panels.*;

import javax.swing.*;

/**
 * Created by gcaron on 13/05/15.
 *
 */
public class MainWindow extends JFrame implements GameConstants {

    private final PanelGame game;
    private final PanelMenu menu;
    private final PanelLose lose;
    private final PanelHelp help;
    private final PanelHighScore high;

    public MainWindow(MainModel model) {
        this.setTitle("Snake");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocation(280,150);
        this.setResizable(false);
        this.game = new PanelGame(model);
        this.menu = new PanelMenu(model);
        this.lose = new PanelLose(model, this);
        this.high = new PanelHighScore(model);
        this.help = new PanelHelp(model);

        setContentPane(menu);
        this.pack();
        this.setVisible(true);
    }

    public PanelGame getGamePanel() {
        return game;
    }

    public PanelMenu getMenuPanel() {
        return menu;
    }

    public PanelLose getLosePanel() {
        return lose;
    }

    public PanelHighScore getHighScorePanel() {
        return high;
    }

    public PanelHelp getHelpPanel() {
        return help;
    }

    public void switchPanel(JPanel to) {
        getContentPane().setVisible(false);
        to.setVisible(true);
        to.repaint();
        setContentPane(to);
        to.requestFocus();
        pack();
    }

}
