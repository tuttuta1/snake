package snake.view;

import snake.model.GlobalConstants;

import java.awt.*;

/**
 * Constantes de vue
 */
public interface ViewConstants {

    public static final byte HEAD_DIAMETER = GlobalConstants.HEAD_RAY * 2;
    public static final byte WALL_DIAMETER = GlobalConstants.RAY_WALL * 2;
    public static final byte ECART_EYES = HEAD_DIAMETER / 3;
    public static final byte DIAMETER_EYES = ECART_EYES / 2;
    public static final byte RAY_EYES = DIAMETER_EYES / 2;
    public static final double ANGLE_EYES = Math.toRadians(30);

    public final static Color GAME_COLOR = new Color(50, 136, 152);
    public final static Color[] HEAD_COLOR = {new Color(0, 153, 76), new Color(204, 0, 204)};
    public final static Color[] TAIL_COLOR = {new Color(0, 204, 102), new Color(255, 0, 255)};

    public final static Color WALL_COLOR = new Color(0, 0, 0, 200);
    public final static Color HOVER_COLOR = new Color(250, 250, 250, 150);
    public final static Color PAUSE_BLACK = new Color(0, 0, 0, 238);
    public final static Color SCORE_BLUE = new Color(GAME_COLOR.getRed(), GAME_COLOR.getGreen(), GAME_COLOR.getBlue(), 100);

    public final static String[][] TEXT_MENU = {{"Highscore", "Play", "Quit"}, {"One Player", "Two Player", "Back"}};

    public final int WINDOW_WIDTHd2 = GlobalConstants.WINDOW_WIDTH / 2;
    public final int WINDOW_HEIGHTd2 = GlobalConstants.WINDOW_HEIGHT / 2;

    public static final byte MAX_POS = 25;
    public static final byte MIN_POS = 0;
    public static final byte SPEED = 4;
    public static final byte SPEED_OPACITY = 10;

    public static final short MAIN_CIRCLE_RAY = 150;
    public static final short MAIN_CIRCLE_DIAMETER = 300;

    public static final float RAY_LOGO_A = (float) Math.toRadians(150);
    public static final float RAY_LOGO_B = (float) Math.toRadians(-90);
    public static final float RAY_LOGO_BETWEEN_LETTERS = (float) Math.toRadians(-10);

    public static final byte SPEED_STEP_BUTTON = 4;
}
