package snake;

import snake.control.MainControl;
import snake.model.MainModel;

/**
 * Main du programme
 */

public class MainSnake {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainModel model = new MainModel();
                MainControl.initControl(model);
            }
        });
    }
}
